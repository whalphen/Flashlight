EESchema Schematic File Version 2
LIBS:wbhLibrary
LIBS:power
LIBS:device
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:special
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:Flashlite-cache
EELAYER 25 0
EELAYER END
$Descr USLetter 11000 8500
encoding utf-8
Sheet 1 1
Title ""
Date "26 aug 2015"
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L BATTERY BT1
U 1 1 55DCF411
P 5000 3900
F 0 "BT1" V 5000 4200 60  0000 C CNN
F 1 "9 Volt Battery" H 5000 3650 60  0000 C CNN
F 2 "~" H 5000 3900 60  0000 C CNN
F 3 "~" H 5000 3900 60  0000 C CNN
F 4 "Keystone Elect 967" V 5000 3900 60  0001 C CNN "Part Number"
	1    5000 3900
	0    1    1    0   
$EndComp
$Comp
L SPST SW1
U 1 1 55DCF43C
P 6050 3450
F 0 "SW1" H 6050 3650 70  0000 C CNN
F 1 "SPST" H 6050 3300 70  0000 C CNN
F 2 "~" H 6050 3450 60  0000 C CNN
F 3 "~" H 6050 3450 60  0000 C CNN
F 4 "Panasonic EVQ-PAC" H 6050 3450 60  0001 C CNN "Part Number"
	1    6050 3450
	1    0    0    -1  
$EndComp
$Comp
L LED D1
U 1 1 55DCF44F
P 6050 4300
F 0 "D1" H 6050 4450 60  0000 C CNN
F 1 "LED" H 6050 4150 60  0000 C CNN
F 2 "~" H 6050 4300 60  0000 C CNN
F 3 "~" H 6050 4300 60  0000 C CNN
F 4 "Vishay VLHW5100" H 6050 4300 60  0001 C CNN "Part Number"
	1    6050 4300
	-1   0    0    1   
$EndComp
$Comp
L RESISTORZ R1
U 1 1 55DCF49F
P 6900 3700
F 0 "R1" H 7150 3500 60  0000 C CNN
F 1 "RESISTORZ" H 7200 3550 40  0001 C CNN
F 2 "~" H 6900 3700 60  0000 C CNN
F 3 "~" H 6900 3700 60  0000 C CNN
	1    6900 3700
	1    0    0    -1  
$EndComp
$Comp
L TP TP3
U 1 1 55DCF4AE
P 6700 4400
F 0 "TP3" H 6700 4650 60  0001 C CNN
F 1 "Test Point 3" H 6700 4550 60  0000 C CNN
F 2 "~" H 6700 4400 60  0000 C CNN
F 3 "~" H 6700 4400 60  0000 C CNN
	1    6700 4400
	-1   0    0    1   
$EndComp
$Comp
L TP TP1
U 1 1 55DCF4F2
P 5300 3350
F 0 "TP1" H 5300 3600 60  0001 C CNN
F 1 "Test Point 1" H 5300 3500 60  0000 C CNN
F 2 "~" H 5300 3350 60  0000 C CNN
F 3 "~" H 5300 3350 60  0000 C CNN
	1    5300 3350
	1    0    0    -1  
$EndComp
$Comp
L TP TP4
U 1 1 55DCF4FC
P 5300 4400
F 0 "TP4" H 5300 4650 60  0001 C CNN
F 1 "Test Point 4" H 5300 4550 60  0000 C CNN
F 2 "~" H 5300 4400 60  0000 C CNN
F 3 "~" H 5300 4400 60  0000 C CNN
	1    5300 4400
	-1   0    0    1   
$EndComp
$Comp
L TP TP2
U 1 1 55DCF52A
P 6650 3350
F 0 "TP2" H 6650 3600 60  0001 C CNN
F 1 "Test Point 2" H 6650 3500 60  0000 C CNN
F 2 "~" H 6650 3350 60  0000 C CNN
F 3 "~" H 6650 3350 60  0000 C CNN
	1    6650 3350
	1    0    0    -1  
$EndComp
Wire Wire Line
	5000 3600 5000 3450
Wire Wire Line
	5000 3450 5550 3450
Connection ~ 5300 3450
Wire Wire Line
	6550 3450 6900 3450
Wire Wire Line
	6900 3450 6900 3650
Connection ~ 6650 3450
Wire Wire Line
	6900 4300 6900 4150
Wire Wire Line
	6250 4300 6900 4300
Connection ~ 6700 4300
Wire Wire Line
	5000 4300 5850 4300
Wire Wire Line
	5000 4300 5000 4200
Connection ~ 5300 4300
Text Notes 5600 2800 0    80   ~ 16
LED Flashlight
$EndSCHEMATC
